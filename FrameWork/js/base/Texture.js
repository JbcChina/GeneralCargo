class Bitmap extends Resource {
  constructor() {
      super();
      this._width = -1;
      this._height = -1;
  }
  get width() {
      return this._width;
  }
  set width(width) {
      this._width = width;
  }
  get height() {
      return this._height;
  }
  set height(height) {
      this._height = height;
  }
  _getSource() {
      throw "Bitmap: must override it.";
  }
}

(function (FilterMode) {
  FilterMode[FilterMode["Point"] = 0] = "Point";
  FilterMode[FilterMode["Bilinear"] = 1] = "Bilinear";
  FilterMode[FilterMode["Trilinear"] = 2] = "Trilinear";
})(exports.FilterMode || (exports.FilterMode = {}));

(function (TextureFormat) {
  TextureFormat[TextureFormat["R8G8B8"] = 0] = "R8G8B8";
  TextureFormat[TextureFormat["R8G8B8A8"] = 1] = "R8G8B8A8";
  TextureFormat[TextureFormat["R5G6B5"] = 16] = "R5G6B5";
  TextureFormat[TextureFormat["Alpha8"] = 2] = "Alpha8";
  TextureFormat[TextureFormat["DXT1"] = 3] = "DXT1";
  TextureFormat[TextureFormat["DXT5"] = 4] = "DXT5";
  TextureFormat[TextureFormat["ETC1RGB"] = 5] = "ETC1RGB";
  TextureFormat[TextureFormat["PVRTCRGB_2BPPV"] = 9] = "PVRTCRGB_2BPPV";
  TextureFormat[TextureFormat["PVRTCRGBA_2BPPV"] = 10] = "PVRTCRGBA_2BPPV";
  TextureFormat[TextureFormat["PVRTCRGB_4BPPV"] = 11] = "PVRTCRGB_4BPPV";
  TextureFormat[TextureFormat["PVRTCRGBA_4BPPV"] = 12] = "PVRTCRGBA_4BPPV";
  TextureFormat[TextureFormat["R32G32B32A32"] = 15] = "R32G32B32A32";
  TextureFormat[TextureFormat["R16G16B16A16"] = 17] = "R16G16B16A16";
})(exports.TextureFormat || (exports.TextureFormat = {}));

(function (WarpMode) {
  WarpMode[WarpMode["Repeat"] = 0] = "Repeat";
  WarpMode[WarpMode["Clamp"] = 1] = "Clamp";
})(exports.WarpMode || (exports.WarpMode = {}));

class BaseTexture extends Bitmap {
  constructor(format, mipMap) {
      super();
      this._wrapModeU = exports.WarpMode.Repeat;
      this._wrapModeV = exports.WarpMode.Repeat;
      this._filterMode = exports.FilterMode.Bilinear;
      this._readyed = false;
      this._width = -1;
      this._height = -1;
      this._format = format;
      this._mipmap = mipMap;
      this._anisoLevel = 1;
      this._glTexture = LayaGL.instance.createTexture();
  }
  get mipmap() {
      return this._mipmap;
  }
  get format() {
      return this._format;
  }
  get wrapModeU() {
      return this._wrapModeU;
  }
  set wrapModeU(value) {
      if (this._wrapModeU !== value) {
          this._wrapModeU = value;
          (this._width !== -1) && (this._setWarpMode(LayaGL.instance.TEXTURE_WRAP_S, value));
      }
  }
  get wrapModeV() {
      return this._wrapModeV;
  }
  set wrapModeV(value) {
      if (this._wrapModeV !== value) {
          this._wrapModeV = value;
          (this._height !== -1) && (this._setWarpMode(LayaGL.instance.TEXTURE_WRAP_T, value));
      }
  }
  get filterMode() {
      return this._filterMode;
  }
  set filterMode(value) {
      if (value !== this._filterMode) {
          this._filterMode = value;
          ((this._width !== -1) && (this._height !== -1)) && (this._setFilterMode(value));
      }
  }
  get anisoLevel() {
      return this._anisoLevel;
  }
  set anisoLevel(value) {
      if (value !== this._anisoLevel) {
          this._anisoLevel = Math.max(1, Math.min(16, value));
          ((this._width !== -1) && (this._height !== -1)) && (this._setAnisotropy(value));
      }
  }
  get mipmapCount() {
      return this._mipmapCount;
  }
  get defaulteTexture() {
      throw "BaseTexture:must override it.";
  }
  _getFormatByteCount() {
      switch (this._format) {
          case exports.TextureFormat.R8G8B8:
              return 3;
          case exports.TextureFormat.R8G8B8A8:
              return 4;
          case exports.TextureFormat.R5G6B5:
              return 1;
          case exports.TextureFormat.Alpha8:
              return 1;
          case exports.TextureFormat.R16G16B16A16:
              return 2;
          case exports.TextureFormat.R32G32B32A32:
              return 4;
          default:
              throw "Texture2D: unknown format.";
      }
  }
  _isPot(size) {
      return (size & (size - 1)) === 0;
  }
  _getGLFormat() {
      var glFormat;
      var gl = LayaGL.instance;
      var gpu = LayaGL.layaGPUInstance;
      switch (this._format) {
          case exports.TextureFormat.R8G8B8:
          case exports.TextureFormat.R5G6B5:
              glFormat = gl.RGB;
              break;
          case exports.TextureFormat.R8G8B8A8:
              glFormat = gl.RGBA;
              break;
          case exports.TextureFormat.Alpha8:
              glFormat = gl.ALPHA;
              break;
          case exports.TextureFormat.R32G32B32A32:
          case exports.TextureFormat.R16G16B16A16:
              glFormat = gl.RGBA;
              break;
          case exports.TextureFormat.DXT1:
              if (gpu._compressedTextureS3tc)
                  glFormat = gpu._compressedTextureS3tc.COMPRESSED_RGB_S3TC_DXT1_EXT;
              else
                  throw "BaseTexture: not support DXT1 format.";
              break;
          case exports.TextureFormat.DXT5:
              if (gpu._compressedTextureS3tc)
                  glFormat = gpu._compressedTextureS3tc.COMPRESSED_RGBA_S3TC_DXT5_EXT;
              else
                  throw "BaseTexture: not support DXT5 format.";
              break;
          case exports.TextureFormat.ETC1RGB:
              if (gpu._compressedTextureEtc1)
                  glFormat = gpu._compressedTextureEtc1.COMPRESSED_RGB_ETC1_WEBGL;
              else
                  throw "BaseTexture: not support ETC1RGB format.";
              break;
          case exports.TextureFormat.PVRTCRGB_2BPPV:
              if (gpu._compressedTexturePvrtc)
                  glFormat = gpu._compressedTexturePvrtc.COMPRESSED_RGB_PVRTC_2BPPV1_IMG;
              else
                  throw "BaseTexture: not support PVRTCRGB_2BPPV format.";
              break;
          case exports.TextureFormat.PVRTCRGBA_2BPPV:
              if (gpu._compressedTexturePvrtc)
                  glFormat = gpu._compressedTexturePvrtc.COMPRESSED_RGBA_PVRTC_2BPPV1_IMG;
              else
                  throw "BaseTexture: not support PVRTCRGBA_2BPPV format.";
              break;
          case exports.TextureFormat.PVRTCRGB_4BPPV:
              if (gpu._compressedTexturePvrtc)
                  glFormat = gpu._compressedTexturePvrtc.COMPRESSED_RGB_PVRTC_4BPPV1_IMG;
              else
                  throw "BaseTexture: not support PVRTCRGB_4BPPV format.";
              break;
          case exports.TextureFormat.PVRTCRGBA_4BPPV:
              if (gpu._compressedTexturePvrtc)
                  glFormat = gpu._compressedTexturePvrtc.COMPRESSED_RGBA_PVRTC_4BPPV1_IMG;
              else
                  throw "BaseTexture: not support PVRTCRGBA_4BPPV format.";
              break;
          default:
              throw "BaseTexture: unknown texture format.";
      }
      return glFormat;
  }
  _setFilterMode(value) {
      var gl = LayaGL.instance;
      WebGLContext.bindTexture(gl, this._glTextureType, this._glTexture);
      switch (value) {
          case exports.FilterMode.Point:
              if (this._mipmap)
                  gl.texParameteri(this._glTextureType, gl.TEXTURE_MIN_FILTER, gl.NEAREST_MIPMAP_NEAREST);
              else
                  gl.texParameteri(this._glTextureType, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
              gl.texParameteri(this._glTextureType, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
              break;
          case exports.FilterMode.Bilinear:
              if (this._mipmap)
                  gl.texParameteri(this._glTextureType, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_NEAREST);
              else
                  gl.texParameteri(this._glTextureType, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
              gl.texParameteri(this._glTextureType, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
              break;
          case exports.FilterMode.Trilinear:
              if (this._mipmap)
                  gl.texParameteri(this._glTextureType, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_LINEAR);
              else
                  gl.texParameteri(this._glTextureType, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
              gl.texParameteri(this._glTextureType, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
              break;
          default:
              throw new Error("BaseTexture:unknown filterMode value.");
      }
  }
  _setWarpMode(orientation, mode) {
      var gl = LayaGL.instance;
      WebGLContext.bindTexture(gl, this._glTextureType, this._glTexture);
      if (this._isPot(this._width) && this._isPot(this._height)) {
          switch (mode) {
              case exports.WarpMode.Repeat:
                  gl.texParameteri(this._glTextureType, orientation, gl.REPEAT);
                  break;
              case exports.WarpMode.Clamp:
                  gl.texParameteri(this._glTextureType, orientation, gl.CLAMP_TO_EDGE);
                  break;
          }
      }
      else {
          gl.texParameteri(this._glTextureType, orientation, gl.CLAMP_TO_EDGE);
      }
  }
  _setAnisotropy(value) {
      var anisotropic = LayaGL.layaGPUInstance._extTextureFilterAnisotropic;
      if (anisotropic) {
          value = Math.max(value, 1);
          var gl = LayaGL.instance;
          WebGLContext.bindTexture(gl, this._glTextureType, this._glTexture);
          value = Math.min(gl.getParameter(anisotropic.MAX_TEXTURE_MAX_ANISOTROPY_EXT), value);
          gl.texParameterf(this._glTextureType, anisotropic.TEXTURE_MAX_ANISOTROPY_EXT, value);
      }
  }
  _disposeResource() {
      if (this._glTexture) {
          LayaGL.instance.deleteTexture(this._glTexture);
          this._glTexture = null;
          this._setGPUMemory(0);
      }
  }
  _getSource() {
      if (this._readyed)
          return this._glTexture;
      else
          return null;
  }
  generateMipmap() {
      if (this._isPot(this.width) && this._isPot(this.height))
          LayaGL.instance.generateMipmap(this._glTextureType);
  }
}
BaseTexture._rgbmRange = 5.0;
BaseTexture.FORMAT_R8G8B8 = 0;
BaseTexture.FORMAT_R8G8B8A8 = 1;
BaseTexture.FORMAT_ALPHA8 = 2;
BaseTexture.FORMAT_DXT1 = 3;
BaseTexture.FORMAT_DXT5 = 4;
BaseTexture.FORMAT_ETC1RGB = 5;
BaseTexture.FORMAT_PVRTCRGB_2BPPV = 9;
BaseTexture.FORMAT_PVRTCRGBA_2BPPV = 10;
BaseTexture.FORMAT_PVRTCRGB_4BPPV = 11;
BaseTexture.FORMAT_PVRTCRGBA_4BPPV = 12;
BaseTexture.RENDERTEXTURE_FORMAT_RGBA_HALF_FLOAT = 14;
BaseTexture.FORMAT_R32G32B32A32 = 15;
BaseTexture.FORMAT_DEPTH_16 = 0;
BaseTexture.FORMAT_STENCIL_8 = 1;
BaseTexture.FORMAT_DEPTHSTENCIL_16_8 = 2;
BaseTexture.FORMAT_DEPTHSTENCIL_NONE = 3;
BaseTexture.FILTERMODE_POINT = 0;
BaseTexture.FILTERMODE_BILINEAR = 1;
BaseTexture.FILTERMODE_TRILINEAR = 2;
BaseTexture.WARPMODE_REPEAT = 0;
BaseTexture.WARPMODE_CLAMP = 1;
