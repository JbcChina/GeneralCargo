import Pool from './Pool'

class DrawImageCmd {
  static create(texture, x, y, width, height) {
      var cmd = Pool.getItemByClass("DrawImageCmd", DrawImageCmd);
      cmd.texture = texture;
      texture._addReference();
      cmd.x = x;
      cmd.y = y;
      cmd.width = width;
      cmd.height = height;
      return cmd;
  }
  recover() {
      this.texture && this.texture._removeReference();
      this.texture = null;
      Pool.recover("DrawImageCmd", this);
  }
  run(context, gx, gy) {
      if (this.texture)
          context.drawTexture(this.texture, this.x + gx, this.y + gy, this.width, this.height);
  }
  get cmdID() {
      return DrawImageCmd.ID;
  }
}
DrawImageCmd.ID = "DrawImage";


class DrawLineCmd {
  static create(fromX, fromY, toX, toY, lineColor, lineWidth, vid) {
      var cmd = Pool.getItemByClass("DrawLineCmd", DrawLineCmd);
      cmd.fromX = fromX;
      cmd.fromY = fromY;
      cmd.toX = toX;
      cmd.toY = toY;
      cmd.lineColor = lineColor;
      cmd.lineWidth = lineWidth;
      cmd.vid = vid;
      return cmd;
  }
  recover() {
      Pool.recover("DrawLineCmd", this);
  }
  run(context, gx, gy) {
      context._drawLine(gx, gy, this.fromX, this.fromY, this.toX, this.toY, this.lineColor, this.lineWidth, this.vid);
  }
  get cmdID() {
      return DrawLineCmd.ID;
  }
}
DrawLineCmd.ID = "DrawLine";

class DrawLinesCmd {
  static create(x, y, points, lineColor, lineWidth, vid) {
      var cmd = Pool.getItemByClass("DrawLinesCmd", DrawLinesCmd);
      cmd.x = x;
      cmd.y = y;
      cmd.points = points;
      cmd.lineColor = lineColor;
      cmd.lineWidth = lineWidth;
      cmd.vid = vid;
      return cmd;
  }
  recover() {
      this.points = null;
      this.lineColor = null;
      Pool.recover("DrawLinesCmd", this);
  }
  run(context, gx, gy) {
      this.points && context._drawLines(this.x + gx, this.y + gy, this.points, this.lineColor, this.lineWidth, this.vid);
  }
  get cmdID() {
      return DrawLinesCmd.ID;
  }
}
DrawLinesCmd.ID = "DrawLines";

class DrawPathCmd {
  static create(x, y, paths, brush, pen) {
      var cmd = Pool.getItemByClass("DrawPathCmd", DrawPathCmd);
      cmd.x = x;
      cmd.y = y;
      cmd.paths = paths;
      cmd.brush = brush;
      cmd.pen = pen;
      return cmd;
  }
  recover() {
      this.paths = null;
      this.brush = null;
      this.pen = null;
      Pool.recover("DrawPathCmd", this);
  }
  run(context, gx, gy) {
      this.paths && context._drawPath(this.x + gx, this.y + gy, this.paths, this.brush, this.pen);
  }
  get cmdID() {
      return DrawPathCmd.ID;
  }
}
DrawPathCmd.ID = "DrawPath";

class DrawPieCmd {
  static create(x, y, radius, startAngle, endAngle, fillColor, lineColor, lineWidth, vid) {
      var cmd = Pool.getItemByClass("DrawPieCmd", DrawPieCmd);
      cmd.x = x;
      cmd.y = y;
      cmd.radius = radius;
      cmd._startAngle = startAngle;
      cmd._endAngle = endAngle;
      cmd.fillColor = fillColor;
      cmd.lineColor = lineColor;
      cmd.lineWidth = lineWidth;
      cmd.vid = vid;
      return cmd;
  }
  recover() {
      this.fillColor = null;
      this.lineColor = null;
      Pool.recover("DrawPieCmd", this);
  }
  run(context, gx, gy) {
      context._drawPie(this.x + gx, this.y + gy, this.radius, this._startAngle, this._endAngle, this.fillColor, this.lineColor, this.lineWidth, this.vid);
  }
  get cmdID() {
      return DrawPieCmd.ID;
  }
  get startAngle() {
      return this._startAngle * 180 / Math.PI;
  }
  set startAngle(value) {
      this._startAngle = value * Math.PI / 180;
  }
  get endAngle() {
      return this._endAngle * 180 / Math.PI;
  }
  set endAngle(value) {
      this._endAngle = value * Math.PI / 180;
  }
}
DrawPieCmd.ID = "DrawPie";

class DrawPolyCmd {
  static create(x, y, points, fillColor, lineColor, lineWidth, isConvexPolygon, vid) {
      var cmd = Pool.getItemByClass("DrawPolyCmd", DrawPolyCmd);
      cmd.x = x;
      cmd.y = y;
      cmd.points = points;
      cmd.fillColor = fillColor;
      cmd.lineColor = lineColor;
      cmd.lineWidth = lineWidth;
      cmd.isConvexPolygon = isConvexPolygon;
      cmd.vid = vid;
      return cmd;
  }
  recover() {
      this.points = null;
      this.fillColor = null;
      this.lineColor = null;
      Pool.recover("DrawPolyCmd", this);
  }
  run(context, gx, gy) {
      this.points && context._drawPoly(this.x + gx, this.y + gy, this.points, this.fillColor, this.lineColor, this.lineWidth, this.isConvexPolygon, this.vid);
  }
  get cmdID() {
      return DrawPolyCmd.ID;
  }
}
DrawPolyCmd.ID = "DrawPoly";

class DrawRectCmd {
  static create(x, y, width, height, fillColor, lineColor, lineWidth) {
      var cmd = Pool.getItemByClass("DrawRectCmd", DrawRectCmd);
      cmd.x = x;
      cmd.y = y;
      cmd.width = width;
      cmd.height = height;
      cmd.fillColor = fillColor;
      cmd.lineColor = lineColor;
      cmd.lineWidth = lineWidth;
      return cmd;
  }
  recover() {
      this.fillColor = null;
      this.lineColor = null;
      Pool.recover("DrawRectCmd", this);
  }
  run(context, gx, gy) {
      context.drawRect(this.x + gx, this.y + gy, this.width, this.height, this.fillColor, this.lineColor, this.lineWidth);
  }
  get cmdID() {
      return DrawRectCmd.ID;
  }
}
DrawRectCmd.ID = "DrawRect";

class DrawCircleCmd {
  static create(x, y, radius, fillColor, lineColor, lineWidth, vid) {
      var cmd = Pool.getItemByClass("DrawCircleCmd", DrawCircleCmd);
      cmd.x = x;
      cmd.y = y;
      cmd.radius = radius;
      cmd.fillColor = fillColor;
      cmd.lineColor = lineColor;
      cmd.lineWidth = lineWidth;
      cmd.vid = vid;
      return cmd;
  }
  recover() {
      this.fillColor = null;
      this.lineColor = null;
      Pool.recover("DrawCircleCmd", this);
  }
  run(context, gx, gy) {
      context._drawCircle(this.x + gx, this.y + gy, this.radius, this.fillColor, this.lineColor, this.lineWidth, this.vid);
  }
  get cmdID() {
      return DrawCircleCmd.ID;
  }
}
DrawCircleCmd.ID = "DrawCircle";

class DrawCurvesCmd {
  static create(x, y, points, lineColor, lineWidth) {
      var cmd = Pool.getItemByClass("DrawCurvesCmd", DrawCurvesCmd);
      cmd.x = x;
      cmd.y = y;
      cmd.points = points;
      cmd.lineColor = lineColor;
      cmd.lineWidth = lineWidth;
      return cmd;
  }
  recover() {
      this.points = null;
      this.lineColor = null;
      Pool.recover("DrawCurvesCmd", this);
  }
  run(context, gx, gy) {
      if (this.points)
          context.drawCurves(this.x + gx, this.y + gy, this.points, this.lineColor, this.lineWidth);
  }
  get cmdID() {
      return DrawCurvesCmd.ID;
  }
}
DrawCurvesCmd.ID = "DrawCurves";