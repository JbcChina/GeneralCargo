
let instansce
const willAwake =  1
const willEnable =  2
const willStart =  3
const willUpdata =  4

const onAwake =  "onAwake"
const onEnable =  "onEnable"
const onStart =  "onStart"
const onUpdate =  "onUpdate"

export default class MonoDriver {
  constructor(){
    if(instansce != null)
      {return instansce;}
      instansce = this
      this.mulCycle = {}
      this.onceCycle = new Array()
      this.onceEnableCycle = {}
  }

  RefreshLifeCycle(){
    let mapOnce = this.onceCycle
    if (mapOnce.length > 0) {
      for (let i = 0; i < mapOnce.length; i++) {
        const element = mapOnce[i];
        this.RefreshOnceCycle(element,onAwake,willAwake,willEnable,true)
      }
      let len = mapOnce.length
      for (let i = 0; i < len; i++) {
        const element = mapOnce[i];
        if (element.active == true) {
          this.RefreshOnceCycle(element,onEnable,willEnable,willStart,this.preExecuteCondition)
        }
      }
      for (let i = 0; i < mapOnce.length; i++) {
        const element = mapOnce[i];
        this.RefreshOnceCycle(element,onStart,willStart,willUpdata,this.preExecuteCondition)
        mapOnce.splice(i,1)
        i--
      }
    }
    this.RefreshEnableCycle()
    this.RefreshUpdateCycle()

  }

  AddCycleFunc(classInstance){
    if(classInstance == null)
    {return}
    if(!classInstance instanceof Mono){return}

    let map = this.mulCycle
    let map2 = this.onceCycle
    if(map[classInstance.monoId] != null)
    {return}
    else{
      map[classInstance.monoId] = classInstance
      map2[classInstance.monoId] = classInstance
      classInstance.monoState = willAwake
    }
  }
  PreCondition(obj){
    if(obj && obj.active == true && obj[funcName] != null){
      return true
    }
    return false
  }

  RefreshOnceCycle(obj,funcName,willState,nextState,preExecuteCondition = true){
    if (preExecuteCondition == true || (preExecuteCondition && preExecuteCondition(obj))) {
      if (obj.monoState == willState) {
        obj[funcName]()
        obj.monoState = nextState
      }
    }
  }

  RefreshEnableCycle(){
    for (const key in this.onceEnableCycle) {
      if (this.onceEnableCycle.hasOwnProperty(key)) {
        const element = this.onceEnableCycle[key];
        if (element.active == true) {
          this.RefreshOnceCycle(element,onEnable,willEnable,willUpdata,true)
        }
      }
    }
  }

  RefreshUpdateCycle(){
    let map = this.mulCycle
    for (const key in map) {
      if (map.hasOwnProperty(key)) {
        const element = map[key];
        if (element.active == true) {
            if(element.monoState == willUpdata){
              if (element[onUpdate] != null) {
                element[onUpdate]()
                element.monoState = willUpdata
              }
            }
        }else{
          this.onceEnableCycle[element.monoId] = element
          element.monoState = willEnable
        }
      }
    }
  }

  RefreshCycle(funcName,willState,nextState,preExecuteCondition = null){

    let map = this.mulCycle
    for (const key in map) {
      if (map.hasOwnProperty(key)) {
        const element = map[key];
        if (element.active == true) {
          if (preExecuteCondition == null || (preExecuteCondition && preExecuteCondition())) {
            if(element.monoState == willState){
              if (element[funcName] != null) {
                element[funcName]()
                element.monoState = nextState
              }
            }
          }
        }else{
          this.onceEnableCycle[element.monoId] = element
          element.monoState = willEnable
        }
      }
    }
  }

}

MonoDriver.getInstance = function(){   
  if(instansce == null) return new MonoDriver() 
  else{return instansce} 
}